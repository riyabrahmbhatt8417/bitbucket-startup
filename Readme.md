##Riya Brahmbhatt ##Student Id: 8688417

**Instructions on How to Use To-do List Application**


---

## Add Value to To-Do

Steps to Do that are following :

1. Type Anything that you want to add in to-do list.
2. Click on Plus Button Beside it.
3. It will add the value to the list.

---

## Delete Value from To-do List

Steps to Do that are following :

1. Click on delete button on the specific value you want to delete.
2. It will remove the value in the to-do list.

---

## Display Task as done or Completed

Steps to Do that are following :

1. Double click on the specific value you want to delete.
2. It will Strike out the value in the to-do list.

---

## How to Run the Application

1. Click on the Index file and run the application after downloading it.


## why selected MIT license

The MIT license is a great choice because it allows you to share your code under a copyleft license without forcing others to expose their proprietary code, it's business friendly and open source friendly while still allowing for monetization.Jul.